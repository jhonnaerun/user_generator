package logic;

import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

public class Generator {
	private Random random;
	private int seed;
	private ResourceBundle messages;
	private String animal, adjective, color;
	private String[] animals = { "abeja", "aguila", "arana", "avispa", "ballena", "bisonte", "bufalo", "burro",
			"caballo", "camello", "canario", "cangrejo", "canguro", "caracol", "cebra", "cerdo", "chimpance", "ciervo",
			"cisne", "cocodrilo", "elefante", "escarabajo", "escorpion", "foca", "gallina", "gallo", "gato",
			"golondrina", "hipopotamo", "hormiga", "jabali", "jirafa", "leon", "loro", "mosca", "mosquito", "oso",
			"oveja", "perdiz", "perro", "pinguino", "pollo", "saltamontes", "serpiente", "tigre", "topo", "toro",
			"tortuga", "vaca", "zorro" };
	private String[] colors = { "amarillo", "ambar", "anil", "azul", "beis", "bermellon", "blanco", "marfil", "burdeos",
			"cafe", "caoba", "caqui", "carmesi", "castano", "celeste", "cereza", "champan", "chartreuse", "cian",
			"cobre", "terracota", "coral", "crema", "fucsia", "granate", "gris", "gualdo", "hueso", "lavanda", "lila",
			"magenta", "marron", "morado", "naranja", "negro", "ocre", "dorado", "pardo", "plata", "purpura", "rojo",
			"rosa", "salmon", "turquesa", "verde", "esmeralda", "aguamarina", "violeta", "vinotinto" };
	
	private String[] Adjectives = { "simpatico", "lento", "rigido", "tenebroso", "habil", "limpio", "fuerte",
			"especial", "impulsivo", "intrepido", "amable", "ansioso", "redondo", "ruidoso", "pequena", "delgado",
			"grande", "enfermo", "grandes", "curioso", "alto", "sutil", "pequeno", "monotono", "estudioso",
			"entrometido", "feliz", "feo", "viejo", "simple", "extraordinario", "imperfecto", "inteligente",
			"maravilloso", "distante", "sensible", "apasionado", "mediano", "colorido", "odioso", "cuidadoso",
			"ambicioso", "arrugado", "bello", "sencillo", "agradable", "gordo", "duro", "desordenado", "horrible",
			"pacifico", "refinado", "celoso", "alargado"};

	public Generator(String seedText) {
		this.seed=seedText.hashCode();	
		random = new Random(seed);

		seleccionIdioma();		
	}

	public String userGenerator() {

		animal = mensajeTexto(animals[random.nextInt(animals.length)]);
		color=mensajeTexto(colors[random.nextInt(colors.length)]);
		adjective=Adjectives[random.nextInt(Adjectives.length)];
		
		return animal+" "+adjective+" "+color;
	}
	
	public String mensajeTexto(String texto) {
		return messages.getString(texto);
	}
	
	public void seleccionIdioma() {
		Locale localizacion; 
		String language="",country="";
		
		if(Locale.getDefault().getLanguage().equals("es")) {
			language=new String("es");
			country= new String("ES");
		}
		if(Locale.getDefault().getLanguage().equals("en")) {
			language=new String("en");
			country= new String("US");
		}
		if(Locale.getDefault().getLanguage().equals("pt")) {
			language=new String("pt");
			country= new String("PTG");
		}
		
		localizacion= new Locale(language, country);
		messages=ResourceBundle.getBundle("resources.Etiquetas", localizacion);
	}
}
